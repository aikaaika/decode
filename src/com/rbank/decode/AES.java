package com.rbank.decode;



import java.util.Base64;
import java.security.Key;
import javax.crypto.Cipher;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.security.MessageDigest;
import java.nio.charset.StandardCharsets;
import javax.crypto.spec.SecretKeySpec;

public class AES
{
    private static final String ENCRYPTION_KEY = "rb4nkS3cr3t";
    private static SecretKeySpec secretKey;
    private static byte[] key;
    
    private AES() {
    }
    
    public static void setKey(final String myKey) {
        MessageDigest sha = null;
        try {
            AES.key = myKey.getBytes(StandardCharsets.UTF_8);
            sha = MessageDigest.getInstance("SHA-1");
            AES.key = sha.digest(AES.key);
            AES.key = Arrays.copyOf(AES.key, 16);
            AES.secretKey = new SecretKeySpec(AES.key, "AES");
        }
        catch (NoSuchAlgorithmException ex) {}
    }
    
    public static synchronized String encrypt(final String strToEncrypt, final String secret) {
        try {
            setKey(secret);
            final Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            cipher.init(1, AES.secretKey);
            return Base64.getEncoder().encodeToString(cipher.doFinal(strToEncrypt.getBytes(StandardCharsets.UTF_8)));
        }
        catch (Exception ex) {
            return null;
        }
    }
    
    public static synchronized String decrypt(final String strToDecrypt, final String secret) {
        try {
            setKey(secret);
            final Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
            cipher.init(2, AES.secretKey);
            return new String(cipher.doFinal(Base64.getDecoder().decode(strToDecrypt)));
        }
        catch (Exception ex) {
            return null;
        }
    }
}
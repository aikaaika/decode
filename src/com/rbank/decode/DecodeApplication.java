package com.rbank.decode;

import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JTextArea;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;




public class DecodeApplication extends JFrame implements ActionListener { 
	
	 /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private JMenuBar mb;
	 private JMenu file;
	 private JMenuItem open;
	 private JTextArea t1;
	 private JTextArea t2;
	 
	 public DecodeApplication () {
		  (this.open = new JMenuItem("Open File")).addActionListener(this);
	        (this.file = new JMenu("File")).add(this.open);
	        (this.mb = new JMenuBar()).setBounds(0, 0, 800, 20);
	        this.mb.add(this.file);
	        (this.t1 = new JTextArea(10, 20)).setBounds(0, 20, 800, 800);
	        this.t1.setEditable(false);
	        this.t1.setVisible(true);
	        (this.t2 = new JTextArea(10, 20)).setBounds(0, 20, 800, 800);
	        this.t2.setEditable(false);
	        this.t2.setVisible(false);
	        this.add(this.mb);
	        this.add(this.t1);
	        this.add(this.t2); 
	 }

	public static void main(String[] args) {
		
		try {
			UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
			
		          
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (UnsupportedLookAndFeelException e) {
			e.printStackTrace();
		} 
		
		 EventQueue.invokeLater(new Runnable() {
	            @Override
	            public void run() {
	            	DecodeApplication bg = new DecodeApplication();
	    		    bg.setSize(800, 400);
	    		    bg.setLayout(null);
	    		    bg.setVisible(true);
	            }
	        });
//		 final String key = "rb4nkS3cr3t";
//		String mobile = AES.decrypt("kPWUSVT/pV9u0CNfn3DU1A==", key);
//		String lastname  = AES.decrypt("cwBySgtgdynDFuCp6B9U1A==", key);
//		String firstname  = AES.decrypt("xCZSSMSoLu5cmR0E0yXakw==", key);
//		String password  = AES.decrypt("$2a$10$UHIxmg0ON/LWidF/pw3NZ.v3p.UDxoBs3ZT8hFyDZXcMBh2FRmOVC", key);
//		String middlename  = AES.decrypt("YBAfJt+Dj558lI60PRLYOA==", key);
//		String email  = AES.decrypt("2GrBAMsqvXvAmnqd+iafX762XY+VyKflXQT/neZ7bQvabbNnstTLpc6cL2qpQZun", key);
//		System.out.println(mobile);
//		System.out.println(lastname + "= lastname");
//		System.out.println(firstname + "= firstname");
//		System.out.println(password + "= password");
//		System.out.println(middlename + "= middlename");
//		System.out.println(email + "= email");
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String[] data = null;
        final String line = "";
        final String splitBy = ",";
        final String key = "rb4nkS3cr3t";
        final int count = 0;
        this.t1.selectAll();
        this.t1.replaceSelection("");
        if (e.getSource() == this.open) {
            final JFileChooser fc = new JFileChooser();
            final int i = fc.showOpenDialog(this);
            if (i == 0) {
                final File f = fc.getSelectedFile();
                final String filepath = f.getPath();
                this.t2.append("CIF,FIRSTNAME,MIDDLENAME,LASTNAME,BIRTHDAY,EMAIL ADDRESS,CONTACT NUMBER \n");
                try {
                    final BufferedReader br = new BufferedReader(new FileReader(filepath));
                    br.readLine();
                    String text = null;
                    
                    final FileWriter writer = new FileWriter(getFileNameWithoutExtension(f)+".csv", false);
                    final BufferedWriter bufferedWriter = new BufferedWriter(writer);
                    while ((text = br.readLine()) != null) {
                        final String strNew = text.replace("\"", "");
                        data = strNew.split(splitBy);
                        this.t2.append(String.valueOf(data[0]) + "," + AES.decrypt(data[1], key) + "," + AES.decrypt(data[2], key) + "," + AES.decrypt(data[3], key) + "," + data[4] + "," + AES.decrypt(data[5], key) + "," + AES.decrypt(data[6], key) + "\n");
                    }
                    bufferedWriter.append((CharSequence)this.t2.getText());
                    bufferedWriter.close();
                    this.t1.setText("Done!");
                    br.close();
                }
                catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
        }
		
	}
	
	
	private static String getFileNameWithoutExtension(File file) {
        String fileName = "";
 
        try {
            if (file != null && file.exists()) {
                String name = file.getName();
                fileName = name.replaceFirst("[.][^.]+$", "");
              
            }
        } catch (Exception e) {
            e.printStackTrace();
            fileName = "";
        }
 
        return fileName;
 
    }

}
